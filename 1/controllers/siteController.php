<?php
  
    function actionSalida(){
        //renderizo la vista mostrar
        //y le paso como parametro el nombre
        render("mostrar",[
            "nombre" => $_GET["nombre"]
        ]);
    }
    
    function actionEntrada(){
        //renderizo la vista formulario
        //y pàso como parametro el nombre vacio
        render("formulario",[
            "nombre" => ""
        ]);
    }
    
    function actionFormulario(){
        if(isset($_GET["boton"])){
            //si he pulsado el boton del formulario
            //llamo a la accion salida
            actionSalida();
        }else{
            //si no he pulsado el boton 
            //llamo a la accion entrada
            actionEntrada();
        }
    }
    
    function actionIndex(){
        render("index",[]);
    }

