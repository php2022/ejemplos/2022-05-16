<?php   
/**
 * inicio->accion index->vista inicio
 * listar->accion listar->vista mostrar
 * listar todo->accion todo->vista todo
 */
    function actionIndex(){
        render("inicio",//nombre de la vista
                [
                    "mensaje"=>"Ejemplo de aplicación con datos en MVC"
                ]//parametros que debe mostrar la vista
                );
    }
    
    function actionListar(){
        global $datos;//tengo dsiponibles los datos en la funcion
        //quito el campo foto de todos los registros
        //le mando a la vista todos los datos sin el campo fotos
        foreach($datos as $indice=>$registro){
            unset($datos[$indice]["foto"]);
        }
        render("mostrar",[
            "objetos"=>$datos,
            "campos"=>["Referencia","Descripción"],
            "acciones"=>true,
        ]);
    }
    
    function actionTodo(){
        global $datos;
        //le mando a la vista todos los datos
        foreach($datos as $indice=>$registro){
            //"../imgs/1.jpg"
            $ruta=$datos[$indice]["foto"];
            //añado la etiqueta img a la ruta
            $datos[$indice]["foto"]="<img src=\"$ruta\">";
            
        }
        render("mostrar",[
            "objetos"=>$datos,
            "campos"=>["Referencia","Descripción","Imagen del producto"],
            "acciones"=>false,
        ]);
    }
    
    function actionMensaje(){
        render("hola",[
           "titulo"=>"Bienvenidos",
            "texto"=>"hola clase"
        ]);
    }
    
