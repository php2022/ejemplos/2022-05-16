<?php
    require "vendor/aplicacion.php";
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <ul>
            <li>
                <a href="./index">Index</a>
            </li>
            <li>
                <a href="./quienes">¿Quiénes somos?</a>
            </li>
            <li>
                <a href="./donde">¿Dónde estamos?</a>
            </li>
            <li>
                <a href="./conocenos">Conócenos</a>
            </li>
            <li>
                <a href="./contacto">Contacto</a>
            </li>
        </ul>
        <?php
        //tengo una variable accion que tiene el nombre de la accion a ejecutar
        //accion="actionIndex"
        
        $accion();
        
        
        ?>
    </body>
</html>
