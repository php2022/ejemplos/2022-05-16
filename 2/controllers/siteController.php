<?php   
    function actionIndex(){
        render("index",[]);
    }
    
    function actionQuienes(){
        $componentes=[
            "alumno1","alumno2","alumno3","Jose","Ana"
        ];
        render("equipo",[
            "equipo"=>$componentes
        ]);
    }
    
    function actionDonde(){
        render("mapa",[]);
    }
    
    function actionConocenos(){
        render("conocer",[
            "titulo"=>"empresa de formacion actual",
            "descripcion"=>"descripcion de la empresa",
            "foto"=>"../imgs/empresa.jpg"
        ]);
    }

    function actionContacto(){
        //es la primera vez que cargo la vista del formulario
        if(!isset($_GET["boton"])){
            render("formulario",[]);
        }else{
            //cuando he pulsado el boton
            render("mensaje",[]);
        }
        
    }
